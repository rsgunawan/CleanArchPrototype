﻿using System.Collections.Generic;
using System.ComponentModel;
using Bpc.UserInteraction.Contracts.Checklist;
using Bpc.UserInteraction.Users;

namespace Bpc.UserInteraction.Contracts.ChecklistPage
{
    public interface IChecklistPageLogic : INotifyPropertyChanged
    {
        IUser SelectedUser { get; set; }
        bool IsUserSelectorActive { get; }
        IList<IUser> UserSelection { get; }

        IChecklist Checklist { get; set; }
        bool IsChecklistViewerActive { get; }
        void Load();
    }
}