using System.ComponentModel;

namespace Bpc.UserInteraction.Contracts.Checklist
{
    public interface IAnswer : INotifyPropertyChanged
    {
        int Id { get; set; }
        bool? Yes { get; set; }
        string Notes { get; set; }
        bool IsNotesEnabled { get; }
    }
}