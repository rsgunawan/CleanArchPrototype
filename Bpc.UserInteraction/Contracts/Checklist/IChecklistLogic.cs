using Bpc.UserInteraction.Users;

namespace Bpc.UserInteraction.Contracts.Checklist
{
    public interface IChecklistLogic
    {
        IChecklist GetChecklistForUser(IUser selectedUser);
        bool SaveAnswer(IUser selectedUser, IAnswer answer);
    }
}