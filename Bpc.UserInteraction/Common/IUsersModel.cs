﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using Bpc.UserInteraction.Users;

namespace Bpc.UserInteraction.Common
{
    internal interface IUsersModel : IList<IUser>, INotifyPropertyChanged, INotifyCollectionChanged
    {
        void Load();
    }
}