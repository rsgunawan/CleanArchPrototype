﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Bpc.UserInteraction.Contracts.Common;
using PropertyChanged;

namespace Bpc.UserInteraction.Common.SelectedItem
{
    [ImplementPropertyChanged]
    class SelectedItemBag<T> : ISelectedItemBag<T>
    {
        public T SelectedItem { get; set; }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}