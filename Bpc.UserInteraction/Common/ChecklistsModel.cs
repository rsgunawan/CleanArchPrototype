﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bpc.UserInteraction.Checklist;
using Bpc.UserInteraction.Contracts.Checklist;

namespace Bpc.UserInteraction.Common
{
    class ChecklistsModel
    {
        public ObservableCollection<IChecklist> Checklists { get; }
    }
}
