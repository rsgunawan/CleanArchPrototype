using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using Bpc.UserInteraction.Users;

namespace Bpc.UserInteraction.Common
{
    internal class UsersModel : IUsersModel
    {
        private readonly IUserPersistor _userPersistor;

        private readonly ObservableCollection<IUser> _users = new ObservableCollection<IUser>();
        
        public UsersModel(IUserPersistor userPersistor)
        {
            _userPersistor = userPersistor;
        }

        public void Load()
        {
            _users.Clear();
            foreach (var user in _userPersistor.LoadUsers())
            {
                _users.Add(user);
            }
        }


        #region IList<IUser> implementation

        public IEnumerator<IUser> GetEnumerator()
        {
            return _users.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) _users).GetEnumerator();
        }

        public void Add(IUser item)
        {
            _users.Add(item);
        }

        public void Clear()
        {
            _users.Clear();
        }

        public bool Contains(IUser item)
        {
            return _users.Contains(item);
        }

        public void CopyTo(IUser[] array, int arrayIndex)
        {
            _users.CopyTo(array, arrayIndex);
        }

        public bool Remove(IUser item)
        {
            return _users.Remove(item);
        }

        public int Count => _users.Count;

        public bool IsReadOnly => false;

        public int IndexOf(IUser item)
        {
            return _users.IndexOf(item);
        }

        public void Insert(int index, IUser item)
        {
            _users.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _users.RemoveAt(index);
        }

        public IUser this[int index]
        {
            get => _users[index];
            set => _users[index] = value;
        }

        #endregion

        #region Implementation of INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Implementation of INotifyCollectionChanged

        public event NotifyCollectionChangedEventHandler CollectionChanged
        {
            add => _users.CollectionChanged += value;
            remove => _users.CollectionChanged -= value;
        }

        #endregion
    }
}