using Bpc.UserInteraction.Contracts.Checklist;
using Bpc.UserInteraction.Users;

namespace Bpc.UserInteraction.Checklist
{
    internal class ChecklistLogic : IChecklistLogic
    {
        private readonly IChecklistPersistor _persistor;

        public ChecklistLogic(IChecklistPersistor persistor)
        {
            _persistor = persistor;
        }
        public IChecklist GetChecklistForUser(IUser selectedUser)
        {
            return _persistor.GetChecklistForUser(selectedUser);
        }

        public bool SaveAnswer(IUser user, IAnswer answer)
        {
            return _persistor.SaveAnswer(user, answer);
        }
    }
}