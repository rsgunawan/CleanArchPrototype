using System.Collections.Generic;
using Bpc.UserInteraction.Contracts.Checklist;

namespace Bpc.UserInteraction.Checklist
{
    internal class Checklist : IChecklist
    {
        public int Id { get; set; }
        public IReadOnlyList<IQuestion> Questions { get; set; }

        public Checklist(int id, IReadOnlyList<IQuestion> questions)
        {
            Id = id;
            Questions = questions;
        }
    }
}