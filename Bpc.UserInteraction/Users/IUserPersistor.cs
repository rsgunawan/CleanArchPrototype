using System.Collections.Generic;

namespace Bpc.UserInteraction.Users
{
    public interface IUserPersistor
    {
        IEnumerable<IUser> LoadUsers();
    }
}