﻿//using Bpc.Users;
//using Moq;
//using NUnit.Framework;

//namespace Bpc.Tests.Common
//{
//    [TestFixture]
//    public class UsersModelTests : AssertionHelper
//    {
//        [Test(Description = "Users model starts out empty.")]
//        public void StartsEmpty()
//        {
//            var userPersistorMock = new Mock<IUserPersistor>();

//            // we're testing the initial state of the collection.
//            // so we have to disable Resharper's warning about the
//            // collection never being updated.
//            // ReSharper disable once CollectionNeverUpdated.Local
//            var target = new UsersModel(userPersistorMock.Object);

//            Assert.IsTrue(target.Count == 0);
//        }

//        [Test(Description = "Users model loads users through IUserPersistor.")]
//        public void UserModelLoadsUsers()
//        {
//            var usersMock = new[]
//            {
//                new UserMock(0, "User 0", true),
//                new UserMock(1, "User 1", false),
//                new UserMock(2, "User 2", false),
//                new UserMock(3, "User 3", true),
//            };
//            var userPersistorMock = new Mock<IUserPersistor>();
//            userPersistorMock.Setup(o => o.LoadUsers())
//                .Returns(usersMock);

//            var target = new UsersModel(userPersistorMock.Object);

//            target.Load();

//            Expect(target, Is.EquivalentTo(usersMock));
//        }

//        [Test(Description = "LoadUsers always refreshes the collection.")]
//        public void LoadUsersRefreshUsers()
//        {

//            var userPersistorMock = new Mock<IUserPersistor>();
//            userPersistorMock.Setup(o => o.LoadUsers())
//                .Returns(new[]
//                {
//                    new UserMock(0, "User 0", true),
//                    new UserMock(1, "User 1", false),
//                    new UserMock(2, "User 2", false),
//                    new UserMock(3, "User 3", true),
//                });

//            var target = new UsersModel(userPersistorMock.Object);

//            target.Load();

//            var newUsers = new[]
//            {
//                new UserMock(4, "User 4", true),
//                new UserMock(5, "User 5", false)
//            };

//            userPersistorMock.Setup(o => o.LoadUsers())
//                .Returns(newUsers);

//            target.Load();

//            Expect(target, Is.EquivalentTo(newUsers));
//        }
//    }

//    class UserMock : IUser
//    {
//        public int Id { get; }
//        public string Name { get; }
//        public bool CanCreateBpc { get; }

//        public UserMock(int id, string name, bool canCreateBpc)
//        {
//            Id = id;
//            Name = name;
//            CanCreateBpc = canCreateBpc;
//        }
//    }
//}
