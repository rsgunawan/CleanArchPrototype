﻿using Bpc.UserInteraction.Users;

namespace Bpc.UserInteraction.Tests.Common
{
    public class UserMock : IUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool CanCreateBpc { get; set; }

        public UserMock(int id, string name, bool canCreateBpc)
        {
            Id = id;
            Name = name;
            CanCreateBpc = canCreateBpc;
        }
    }
}
