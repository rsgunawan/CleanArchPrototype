﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using BoDi;
using Bpc.UserInteraction.Checklist;
using Bpc.UserInteraction.ChecklistPage;
using Bpc.UserInteraction.Common;
using Bpc.UserInteraction.Contracts.Checklist;
using Bpc.UserInteraction.Contracts.ChecklistPage;
using Bpc.UserInteraction.Tests.Common;
using Bpc.UserInteraction.Users;
using FakeItEasy;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Bpc.UserInteraction.Tests.ChecklistPage
{
    [Binding]
    public class ChecklistPageSteps : AssertionHelper
    {
        private readonly IObjectContainer _scenarioContainer; // stored to avoid using static properties.
        private IChecklistPageLogic _target;

        public ChecklistPageSteps(IObjectContainer scenarioContainer)
        {
            _scenarioContainer = scenarioContainer;
        }
        
        [BeforeScenario]
        public void BeforeScenario()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<BpcUserInteractionAutofacModule>();

            var users = new List<IUser>
            {
                new UserMock(0, "user 1", true),
                new UserMock(1, "user 2", false),
            };

            var usersModelMock = A.Fake<IUsersModel>();
            A.CallTo(() => usersModelMock.GetEnumerator())
             .Returns(users.GetEnumerator());

            builder.RegisterInstance(usersModelMock).As<IUsersModel>();

            builder.RegisterInstance(A.Fake<IChecklistPersistor>()).AsImplementedInterfaces();

            var container = builder.Build();
            _scenarioContainer.RegisterInstanceAs(container);
        }

        [Given(@"the checklist page")]
        public void GivenTheChecklistPage()
        {
            var container = _scenarioContainer.Resolve<IContainer>();
            _target = container.Resolve<IChecklistPageLogic>();
        }

        [When(@"I select (.*)")]
        public void WhenISelectAUser(UserSteps.UserType userType)
        {
            var container = _scenarioContainer.Resolve<IContainer>();
            var usersModel = container.Resolve<IUsersModel>();

            var selectedUser = userType == UserSteps.UserType.Bpc 
                ? usersModel.First(u => u.CanCreateBpc) 
                : usersModel.First(u => !u.CanCreateBpc);
            _target.SelectedUser = selectedUser;
        }

        [Given(@"there is no selected user")]
        public void GivenThereIsNoSelectedUser()
        {
            Assert.IsNull(_target.SelectedUser);
        }

        [When(@"the page loads")]
        public void WhenThePageLoads()
        {
            _target.Load();
        }

        [Then(@"the user selector is (inactive|active)")]
        public void ThenTheUserSelectorIsShown(IsActive expectedActive)
        {
            VerifyActive(expectedActive, _target.IsUserSelectorActive);
            if (expectedActive == IsActive.Active)
            {
                Assert.IsTrue(_target.UserSelection.Any());
            }
        }

        public enum IsActive
        {
            Inactive = 0,
            Active
        }

        [Then(@"the checklist viewer is (inactive|active)")]
        public void ThenTheChecklistViewerIsInactive(IsActive expectedActive)
        {
            Assert.IsFalse(_target.IsChecklistViewerActive);
        }

        private void VerifyActive(IsActive expected, bool isActiveActual)
        {
            var assertMethod = expected == IsActive.Active
                ? (Action<bool>)Assert.IsTrue
                : Assert.IsFalse;
            assertMethod(isActiveActual);
        }

        [Then(@"the user selector contains all users")]
        public void ThenTheUserSelectorContainsAllUsers()
        {
            Expect(_target.UserSelection, Is.Not.Empty);
        }

        [Then(@"a user is selected")]
        public void ThenAUserIsSelected()
        {
            Assert.IsNotNull(_target.SelectedUser);
        }
    }
}
