﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bpc.UserInteraction.Checklist;
using Bpc.UserInteraction.ChecklistPage;
using Bpc.UserInteraction.Common.SelectedItem;
using Bpc.UserInteraction.Contracts.Checklist;
using Bpc.UserInteraction.Contracts.ChecklistPage;
using Bpc.UserInteraction.Contracts.Common;
using Bpc.UserInteraction.Tests.Common;
using Bpc.UserInteraction.Users;
using FakeItEasy;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Bpc.UserInteraction.Tests.ChecklistPage
{
    public enum YesNo
    {
        Yes,
        No
    }

    [Binding]
    class QuestionViewerSteps
    {
        private ISelectedItemBag<IUser> _selectedUserBag;
        private IChecklistViewerLogic _checklistViewerLogic;
        private IChecklistLogic _checklistLogic;
        private IChecklist _checklist;
        private IQuestion _question;

        [Given(@"a user with checklist that has questions")]
        public void GivenAUserWithChecklistThatHasQuestions()
        {
            _selectedUserBag = A.Fake<ISelectedItemBag<IUser>>();
            var user = new UserMock(0, "BPC User", true);
            _selectedUserBag.SelectedItem = user;
            _checklistLogic = A.Fake<IChecklistLogic>();
            _checklist = A.Fake<IChecklist>();

            var question = new Question(0, "Question text")
            {
                Answer = new Answer(null, null)
            };
            _question = question;

            A.CallTo(() => _checklist.Questions)
                .Returns(new ReadOnlyCollection<IQuestion>(new List<IQuestion> { question }));

            A.CallTo(() => _checklistLogic.GetChecklistForUser(user))
                .Returns(_checklist);
        }

        [Given(@"a checklist viewer with the user")]
        public void GivenAChecklistViewerWithTheUser()
        {
            _checklistViewerLogic = new ChecklistViewerLogic(_checklistLogic, _selectedUserBag);
        }

        [Then(@"show each question")]
        public void ThenShowEachQuestion()
        {
            Assert.AreSame(_checklist, _checklistViewerLogic.CurrentChecklist);
            Assert.IsTrue(_checklistViewerLogic.IsQuestionListVisible);
        }

        [Given(@"a question in the checklist")]
        public void GivenAQuestionInTheChecklist()
        {
            _question = _checklist.Questions.First();
        }

        [Given(@"I answer ""(.*)""")]
        [When(@"I answer ""(.*)""")]
        public void WhenIAnswer(YesNo yesNo)
        {
            _question.Answer.Yes = yesNo == YesNo.Yes;
        }

        [Then(@"the ""(.*)"" answer should be recorded")]
        public void ThenTheAnswerShouldBeRecorded(YesNo yesNo)
        {
            //Assert.AreEqual(true, _yesAnswered);
            A.CallTo(() => _checklistLogic.SaveAnswer(_selectedUserBag.SelectedItem, _question.Answer))
                .MustHaveHappened();
            // i don't know why _question.Answer.Yes is null
            Assert.AreEqual(yesNo == YesNo.Yes, _question.Answer.Yes);
        }

        [Then(@"the notes entry is not active")]
        public void ThenTheNotesEntryIsNotActive()
        {
            Assert.IsFalse(_question.Answer.IsNotesEnabled);
        }

        [Then(@"the notes entry is active")]
        public void ThenTheNotesEntryIsActive()
        {
            Assert.IsTrue(_question.Answer.IsNotesEnabled);
        }

        private const string newNote = "New notes";
        [When(@"I enter a note")]
        public void WhenIEnterANote()
        {
            _question.Answer.Notes = newNote;
        }

        [Then(@"the note should be recorded")]
        public void ThenTheNoteShouldBeRecorded()
        {
            A.CallTo(() => _checklistLogic.SaveAnswer(_selectedUserBag.SelectedItem, _question.Answer))
                .MustHaveHappened();
            Assert.AreEqual(newNote, _question.Answer.Notes);
        }

    }
}
