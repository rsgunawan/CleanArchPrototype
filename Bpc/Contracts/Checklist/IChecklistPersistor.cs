using Bpc.Contracts.Users;

namespace Bpc.Contracts.Checklist
{
    public interface IChecklistPersistor
    {
        IChecklist Save(IUser user, IChecklist toSave);
        IChecklist Load(IUser user);

        IAnswer LoadAnswer(IUser user, int answerId);
        IAnswer SaveAnswer(IUser user, IAnswer newAnswer);
    }
}