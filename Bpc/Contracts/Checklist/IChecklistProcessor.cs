using Bpc.Contracts.Users;

namespace Bpc.Contracts.Checklist
{
    public interface  IChecklistProcessor
    {
        IChecklist GetChecklistForUser(IUser user);
        IChecklist CreateChecklistForUser(IUser user);

        IAnswer SaveAnswer(IUser user, int answerId, bool? yes, string notes);
    }
}