﻿namespace Bpc.Contracts.EvalEngine.Info
{
    public interface IBinaryExprInfo<T> : IExprInfo<T>
    {
        IExprInfo LeftExpr { get; set; }
        IExprInfo RightExpr { get; set; }
        Operator Operator { get; set; }
    }
}