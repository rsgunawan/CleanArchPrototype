﻿namespace Bpc.Contracts.EvalEngine.Info
{
    public interface IConstantExprInfo<T> : IExprInfo<T>
    {
        T ConstantValue { get; set; }
    }
}