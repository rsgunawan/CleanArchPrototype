﻿using Bpc.Contracts.EvalEngine.Info;
using Bpc.EvalEngine.Expr;

namespace Bpc.Contracts.EvalEngine.Expr
{
    public interface IExprFactory
    {
        IExpr<T> Create<T>(IExprInfo<T> info);
        IExpr CreateNoGeneric(IExprInfo info);
    }
}
