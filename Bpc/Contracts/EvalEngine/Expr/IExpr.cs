﻿using Bpc.EvalEngine;

namespace Bpc.Contracts.EvalEngine.Expr
{
    public interface IExpr
    {
        object Evaluate(IContext context);
    }

    public interface IExpr<out T> : IExpr
    {
        new T Evaluate(IContext context);
    }
}