﻿using System.Collections.Generic;

namespace Bpc.Contracts.Users
{
    public interface IUserPersistor
    {
        IEnumerable<IUser> LoadUsers();
    }
}