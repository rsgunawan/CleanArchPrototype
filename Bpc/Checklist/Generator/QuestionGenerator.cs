﻿using System;
using System.Collections.Generic;
using Bpc.Contracts.Checklist;

namespace Bpc.Checklist.Generator
{
    internal class QuestionGenerator : IQuestionGenerator
    {
        private readonly Func<int, string, IQuestion> _questionFactory;

        public QuestionGenerator(Func<int, string, IQuestion> questionFactory)
        {
            _questionFactory = questionFactory;
        }

        public IEnumerable<IQuestion> GenerateQuestions(string clientName)
        {
            var charIndex = 0;
            foreach (var character in clientName)
            {
                charIndex++;
                yield return _questionFactory(charIndex, character + " question?");
            }
        }
    }
}
