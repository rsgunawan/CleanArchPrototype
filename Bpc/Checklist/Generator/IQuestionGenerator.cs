﻿using System.Collections.Generic;
using Bpc.Contracts.Checklist;

namespace Bpc.Checklist.Generator
{
    internal interface IQuestionGenerator
    {
        IEnumerable<IQuestion> GenerateQuestions(string clientName);
    }
}