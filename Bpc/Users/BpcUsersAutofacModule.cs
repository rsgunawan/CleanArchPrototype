﻿using Autofac;

namespace Bpc.Users
{
    internal class BpcUsersAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<UserProcessor>().AsImplementedInterfaces().InstancePerLifetimeScope();
        }
    }
}
