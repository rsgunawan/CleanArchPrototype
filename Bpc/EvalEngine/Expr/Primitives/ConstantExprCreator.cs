﻿using System;
using Bpc.Contracts.EvalEngine.Expr;
using Bpc.Contracts.EvalEngine.Info;

namespace Bpc.EvalEngine.Expr.Primitives
{
    internal class ConstantExprCreator<T> : IExprCreator<T>
    {
        private readonly Func<T, ConstantExpr<T>> _resolveFunc;

        public ConstantExprCreator(Func<T, ConstantExpr<T>> resolver)
        {
            _resolveFunc = resolver;
        }
        public bool CanHandle(IExprInfo<T> info)
        {
            return info is IConstantExprInfo<T>;
        }

        public IExpr<T> Create(IExprInfo<T> info)
        {
            var constantExprInfo = (IConstantExprInfo<T>)info;
            var output = _resolveFunc(constantExprInfo.ConstantValue);
            return output;
        }
    }
}