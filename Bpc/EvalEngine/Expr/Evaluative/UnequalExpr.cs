﻿using Bpc.Contracts.EvalEngine;
using Bpc.Contracts.EvalEngine.Expr;

namespace Bpc.EvalEngine.Expr.Evaluative
{
    internal delegate UnequalExpr CreateUnequalExpr(IExpr leftExpr, IExpr rightExpr);

    internal class UnequalExpr : BinaryExpr, IExpr<bool>
    {

        public UnequalExpr(IExpr leftExpr, IExpr rightExpr) 
            : base(EvaluateFunc, leftExpr, rightExpr)
        {
        }

        private static bool EvaluateFunc(object leftObj, object rightObj)
        {
            return !Equals(leftObj, rightObj);
        }

        bool IExpr<bool>.Evaluate(IContext context)
        {
            return (bool) Evaluate(context);
        }
    }
}