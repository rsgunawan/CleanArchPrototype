﻿using Bpc.Contracts.EvalEngine;
using Bpc.Contracts.EvalEngine.Expr;

namespace Bpc.EvalEngine.Expr.Evaluative
{
    internal delegate EqualExpr CreateEqualExpr(IExpr leftExpr, IExpr rightExpr);

    internal class EqualExpr: BinaryExpr, IExpr<bool>
    {
        public EqualExpr(IExpr leftExpr, IExpr rightExpr) 
            : base(Equals, leftExpr, rightExpr)
        {
        }

        bool IExpr<bool>.Evaluate(IContext context)
        {
            return (bool)Evaluate(context);
        }
    }
}