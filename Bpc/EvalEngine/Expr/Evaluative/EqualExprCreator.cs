using Bpc.Contracts.EvalEngine.Expr;
using Bpc.Contracts.EvalEngine.Info;

namespace Bpc.EvalEngine.Expr.Evaluative
{
    internal class EqualExprCreator<T> : IExprCreator<T>
    {
        private readonly CreateEqualExpr _equalExprFactoryFunc;
        private readonly IExprFactory _exprFactory;

        public EqualExprCreator(CreateEqualExpr equalExprFactoryFunc, IExprFactory exprFactory)
        {
            _equalExprFactoryFunc = equalExprFactoryFunc;
            _exprFactory = exprFactory;
        }

        public bool CanHandle(IExprInfo<T> info)
        {
            var binaryExprInfo = info as IBinaryExprInfo<bool>;
            return binaryExprInfo?.Operator == Operator.Equal;
        }

        public IExpr<T> Create(IExprInfo<T> info)
        {
            var binaryExprInfo = (IBinaryExprInfo<bool>) info;

            var leftExpr = _exprFactory.CreateNoGeneric(binaryExprInfo.LeftExpr);
            var rightExpr = _exprFactory.CreateNoGeneric(binaryExprInfo.RightExpr);
            var output = (IExpr<T>) _equalExprFactoryFunc(leftExpr, rightExpr);

            return output;
        }
    }
}