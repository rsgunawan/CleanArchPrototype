using System;
using System.Collections.Generic;
using System.Windows;
using AutoMapper;
using Bpc.UserInteraction.Checklist;
using Bpc.UserInteraction.Contracts.Checklist;
using Bpc.UserInteraction.Users;
using Client.BpcServiceReference;

namespace Client.Common
{
    class WcfPersistor : IUserPersistor, IChecklistPersistor
    {
        private readonly IBpcService _bpcService;
        private readonly IMapper _mapper;

        public WcfPersistor(IBpcService bpcService, IMapper mapper)
        {
            _bpcService = bpcService;
            _mapper = mapper;
        }

        public IEnumerable<IUser> LoadUsers()
        {
            var raw = _bpcService.GetUsers();

            var output = _mapper.Map<IEnumerable<IUser>>(raw);

            return output;
        }

        public IChecklist GetChecklistForUser(IUser selectedUser)
        {
            IChecklist output;

            if (selectedUser == null)
            {
                output = null;
            }
            else
            {
                var raw = _bpcService.GetChecklistForUser(selectedUser.Id);

                output = _mapper.Map<IChecklist>(raw);
            }
            return output;
        }

        public bool SaveAnswer(IUser user, IAnswer answer)
        {
            bool output;
            try
            {
                _bpcService.SaveAnswer(user.Id, answer.Id, answer.Yes, answer.Notes);
                output = true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                output = false;
            }
            return output;
        }
    }
}