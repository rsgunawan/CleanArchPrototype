﻿using Autofac;
using AutoMapper;

namespace Client.Common
{
    class ClientCommonAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<ClientCommonAutoMapperProfile>().As<Profile>().InstancePerLifetimeScope();

            builder.RegisterType<User>().AsImplementedInterfaces();

            builder.RegisterType<WcfPersistor>().AsImplementedInterfaces().InstancePerLifetimeScope();
        }
    }
}
