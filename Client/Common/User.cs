﻿using Bpc.UserInteraction.Users;

namespace Client.Common
{
    class User : IUser
    {
        public bool CanCreateBpc { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }
    
}
