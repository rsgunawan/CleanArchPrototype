﻿using System;

namespace Client.Helpers
{
    public interface IResolver
    {
        T Resolve<T>();

        object Resolve(Type type);
    }
}