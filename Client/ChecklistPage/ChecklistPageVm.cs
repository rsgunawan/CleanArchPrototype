﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using AutoMapper;
using Bpc.UserInteraction.ChecklistPage;
using Bpc.UserInteraction.Contracts.Checklist;
using Bpc.UserInteraction.Contracts.ChecklistPage;
using Bpc.UserInteraction.Users;
using Client.Annotations;
using Client.Helpers;
using PropertyChanged;

namespace Client.ChecklistPage
{
    [ImplementPropertyChanged]
    class ChecklistPageVm : INotifyPropertyChanged
    {
        private readonly IChecklistPageLogic _checklistPageLogic;
        private readonly IMapper _mapper;

        public bool IsUserSelectorActive { get; set; }
        public UserVm SelectedUser { get; set; }
        public ObservableCollection<UserVm> UserSelection { get; set; }

        
        public Visibility LoadingVisibility => UserSelection?.Any() ?? false ? Visibility.Collapsed : Visibility.Visible;

        public Visibility PageVisibility => UserSelection?.Any() ?? false ? Visibility.Visible : Visibility.Collapsed;

        public bool IsChecklistViewerActive { get; set; }
        
        public IUser SelectedItem { get; set; }

        // called by Autofac
        public ChecklistPageVm(IChecklistPageLogic checklistPageLogic, IMapper mapper)
        {
            _checklistPageLogic = checklistPageLogic;
            _mapper = mapper;
            _checklistPageLogic.PropertyChanged += (sender, args) => SyncWithChecklistPageLogic();

            Task.Run(() => Initialize());
        }

        public void Initialize()
        {
            _checklistPageLogic.Load();
            SyncWithChecklistPageLogic();
        }

        private void SyncWithChecklistPageLogic()
        {
            RunOn.MainThread(() =>
            {
                if (!UserSelection?.Any() ?? true)
                {
                    _mapper.Map(_checklistPageLogic, this);
                }
                else
                {
                    // if we sync UserSelection again, the SelectedUser object would not be in UserSelection
                    // and WPF will reset the combo box SelectedItem.
                    // So, after we're initialized, just sync the selected user with existing VMs.
                    SelectedUser = UserSelection.FirstOrDefault(vm => vm.Id == _checklistPageLogic.SelectedUser?.Id);
                }
            });
        }

        // ReSharper disable once UnusedMember.Local
        // called by fody
        private void OnSelectedUserChanged()
        {
            var selectedUser= SelectedUser == null 
                ? null 
                : _checklistPageLogic.UserSelection.First(u => u.Id == SelectedUser.Id);

            _checklistPageLogic.SelectedUser = selectedUser;
            SelectedItem = selectedUser;
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
