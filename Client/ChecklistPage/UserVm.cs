﻿using PropertyChanged;

namespace Client.ChecklistPage
{
    [ImplementPropertyChanged]
    class UserVm
    {
        public bool CanCreateBpc { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }

        public string ComboText => $@"{Name}{ (CanCreateBpc ? " (BPC)":string.Empty) }";
    }
}