﻿namespace Client.ChecklistPage
{
    internal class QuestionVm
    {
        public int Order { get; set; }
        public string Text { get; set; }

        public string DisplayText => Text;

        public AnswerVm Answer { get; set; } = new AnswerVm();
    }
}