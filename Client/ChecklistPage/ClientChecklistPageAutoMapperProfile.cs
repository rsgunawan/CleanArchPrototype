﻿using System;
using AutoMapper;
using Bpc.UserInteraction.Checklist;
using Bpc.UserInteraction.ChecklistPage;
using Bpc.UserInteraction.Contracts.Checklist;
using Bpc.UserInteraction.Contracts.ChecklistPage;
using Bpc.UserInteraction.Users;
using Client.Common;

namespace Client.ChecklistPage
{
    public class ClientChecklistPageAutoMapperProfile : Profile
    {
        public ClientChecklistPageAutoMapperProfile(Func<bool?, string, IAnswer> answerFactoryFunc)
        {
            CreateMap<IChecklistPageLogic, ChecklistPageVm>();
            CreateMap<IUser, User>();
            CreateMap<IUser, UserVm>();
            CreateMap<IChecklist, ChecklistViewerVm>();
            CreateMap<IQuestion, QuestionVm>()
                .ForMember(vm => vm.Answer, opt => opt.NullSubstitute(answerFactoryFunc(null, null)));
            CreateMap<IAnswer, AnswerVm>()
                .ForMember(vm => vm.Model, opt =>
                {
                    opt.SetMappingOrder(0);
                    opt.ResolveUsing(a => a);
                })
                .ForAllOtherMembers(opt => opt.SetMappingOrder(100));


            CreateMap<IChecklistViewerLogic, ChecklistViewerVm>()
                .ForMember(vm => vm.Questions, opt =>
                {
                    opt.ResolveUsing(logic => logic.CurrentChecklist?.Questions);
                });
        }
    }
}
