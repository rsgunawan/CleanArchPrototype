﻿using Bpc.UserInteraction.Checklist;
using Bpc.UserInteraction.Contracts.Checklist;
using PropertyChanged;

namespace Client.ChecklistPage
{
    [ImplementPropertyChanged]
    internal class AnswerVm
    {
        public IAnswer Model { get; set; }
        public bool? Yes { get; set; }
        public bool No { get; set; }

        public string Notes { get; set; }

        public bool IsNotesEnabled { get; set; }

        // Called by Fody
        // ReSharper disable once UnusedMember.Local
        private void OnYesChanged()
        {
            No = !Yes ?? false;

            Model.Yes = Yes;
            IsNotesEnabled = Model.IsNotesEnabled;
        }

        // Called by Fody
        // ReSharper disable once UnusedMember.Local
        private void OnNoChanged()
        {
            Yes = !No;
        }

        // Called by Fody
        // ReSharper disable once UnusedMember.Local
        private void OnNotesChanged()
        {
            Model.Notes = Notes;
        }
    }
}