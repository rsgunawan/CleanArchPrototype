using System;
using Bpc.Contracts.Users;
using Bpc.Tests.Common;
using TechTalk.SpecFlow;

namespace Bpc.Tests.Checklist
{
    [Binding]
    internal class UserSteps
    {
        public enum UserType
        {
            Bpc,
            NonBpc
        }

        [StepArgumentTransformation(@"a (.*) user")]
        public UserType UserTypeTransform(string userTypeInput)
        {
            UserType output;

            switch (userTypeInput.ToLower())
            {
                case "bpc":
                    output = UserType.Bpc;
                    break;
                default:
                    output = UserType.NonBpc;
                    break;
            }

            return output;
        }

        [Given(@"I am (.*)")]
        public void CreateUser(UserType userType)
        {
            var testUser = new UserMock(0, $"{userType} Test User", userType == UserType.Bpc);
            ScenarioContext.Current.ScenarioContainer.RegisterInstanceAs(testUser, typeof(IUser));
            Console.WriteLine(testUser.Name);
        }
    }
}