﻿using System;
using System.Linq;
using Autofac;
using Bpc.Contracts.Checklist;
using Bpc.Contracts.Users;
using FakeItEasy;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Bpc.Tests.Checklist
{
    [Binding]
    internal class ChecklistManagementSteps
    {
        private readonly IChecklistProcessor _target;
        private IChecklist _result;
        private Exception _error;
        
        public ChecklistManagementSteps()
        {

            var builder = new ContainerBuilder();

            builder.RegisterModule<BpcAutofacModule>();

            builder.RegisterModule<Mocks.ChecklistMocksAutofacModule>();

            builder.RegisterInstance(A.Fake<IChecklistPersistor>()).AsImplementedInterfaces();
            
            var container = builder.Build();
            _target = container.Resolve<IChecklistProcessor>();
            ScenarioContext.Current.ScenarioContainer.RegisterInstanceAs(container, typeof(IComponentContext));
        }

        [Given(@"there is no checklist for the user")]
        public void GivenThereIsNoChecklistForTheUser()
        {
            var user = ScenarioContext.Current.ScenarioContainer.Resolve<IUser>();
            var checklist = _target.GetChecklistForUser(user);

            // The checklist shouldn't already be created.
            Assert.IsNull(checklist);
        }


        [When(@"the user creates the checklist")]
        public void CreateChecklistForUser()
        {
            var user = ScenarioContext.Current.ScenarioContainer.Resolve<IUser>();

            var container = ScenarioContext.Current.ScenarioContainer.Resolve<IComponentContext>();
            var checklistPersistor = container.Resolve<IChecklistPersistor>();

            A.CallTo(checklistPersistor)
                .Where(call => call.Method.Name == nameof(IChecklistPersistor.Save))
                .WithReturnType<IChecklist>()
                .ReturnsLazily(call => (IChecklist) call.Arguments.First(arg => arg is IChecklist));



            try
            {
                _result = _target.CreateChecklistForUser(user);

                Assert.IsNotNull(_result);
            }
            catch (Exception e)
            {
                _error = e;
            }
        }

        [Given(@"there is (\d+) checklist for the user")]
        public void ThereIsChecklistForUser(int checklistCountExpected)
        {
            var user = ScenarioContext.Current.ScenarioContainer.Resolve<IUser>();

            var container = ScenarioContext.Current.ScenarioContainer.Resolve<IComponentContext>();
            var checklistPersistor = container.Resolve<IChecklistPersistor>();
            A.CallTo(checklistPersistor)
                .Where(call => call.Method.Name == nameof(IChecklistPersistor.Load))
                .WithNonVoidReturnType()
                .Returns(null);

            var checklist = _target.GetChecklistForUser(user);

            if (checklist == null && checklistCountExpected > 0)
            {
                CreateChecklistForUser();
            }
        }

        [Then(@"there is (\d+) checklist for the user")]
        public void UserShouldHaveChecklist(int checklistCountExpected)
        {
            var user = ScenarioContext.Current.ScenarioContainer.Resolve<IUser>();
            var checklist = _target.GetChecklistForUser(user);

            var checklistCountActual = checklist == null
                ? 0
                : 1;

            Assert.AreEqual(checklistCountExpected, checklistCountActual);
        }

        [Given(@"there is an existing checklist for the user")]
        public void GivenThereIsAnExistingChecklistForTheUser()
        {
            var user = ScenarioContext.Current.ScenarioContainer.Resolve<IUser>();
            var checklist = _target.GetChecklistForUser(user);

            if (checklist == null)
            {
                _target.CreateChecklistForUser(user);
            }
        }

        [Then(@"I can access the questions in the checklist")]
        public void ThenICanAccessTheQuestionsInTheChecklist()
        {
            var user = ScenarioContext.Current.ScenarioContainer.Resolve<IUser>();
            var checklist = _target.GetChecklistForUser(user);

            Assert.IsTrue(checklist != null);
            Assert.IsTrue(checklist.Questions.Any());
        }


        [Then(@"the checklist has as many questions as the user's name's length")]
        public void VerifyChecklist()
        {
            var user = ScenarioContext.Current.ScenarioContainer.Resolve<IUser>();
            var checklistResult = _target.GetChecklistForUser(user);

            Assert.AreSame(_result, checklistResult);
            Assert.AreEqual(user.Name.Length, _result.Questions.Count);
        }

        [Then(@"no error is shown")]
        public void CheckNoErrors()
        {
            Assert.IsNull(_error);
        }

        [Then(@"a ""(.*)"" error is shown")]
        public void CheckError(string errorMessage)
        {
            Assert.IsNotNull(_error);
            Assert.IsTrue(_error.Message.Contains(errorMessage));
        }

        #region Helpers
        

        #endregion
    }
}
