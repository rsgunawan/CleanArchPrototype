﻿Feature: ChecklistManagement
	In order to edit or fill a checklist
	as a user
	I need to see the checklist associated with a policy
	
Scenario: Create checklist
	Given I am a BPC user
	And there is 0 checklist for the user
	When the user creates the checklist
	Then no error is shown
	And there is 1 checklist for the user
	And the checklist has as many questions as the user's name's length

Scenario: Checklist already exist, I cannot create another one.
	Given I am a BPC user
	And there is 1 checklist for the user
	When the user creates the checklist
	Then a "Checklist already exist" error is shown

Scenario: User can view an existing checklist	
	Given I am a BPC user
	And there is 1 checklist for the user
	Then I can access the questions in the checklist
	