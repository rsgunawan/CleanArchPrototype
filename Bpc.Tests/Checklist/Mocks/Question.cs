using Bpc.Contracts.Checklist;

namespace Bpc.Tests.Checklist.Mocks
{
    internal class QuestionMock : IQuestion
    {
        public QuestionMock(int order, string text)
        {
            Order = order;
            Text = text;
        }

        public int Order { get; }
        public string Text { get; }

        public IAnswer Answer { get; set; }
    }
}