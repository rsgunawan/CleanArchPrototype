﻿using Autofac;
using Bpc.Contracts.EvalEngine;
using Bpc.Contracts.EvalEngine.Expr;
using Bpc.Contracts.EvalEngine.Info;
using Bpc.EvalEngine.Expr;
using Bpc.Tests.EvalEngine.Mocks;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Bpc.Tests.EvalEngine
{
    [Binding]
    internal class ConditionSteps
    {
        private bool _result;
        private ICondition _target;
        private IExprInfo<bool> _expr;

        [Given(@"a condition with this expression: (.*) (!=|=) (.*)")]
        public void GivenAConditionWithThisExpression(string leftExpr, string op, string rightExpr)
        {
            _expr = new BinaryExprInfo<bool>
            {
                LeftExpr = new ConstantExprInfo<string> {ConstantValue = leftExpr},
                RightExpr = new ConstantExprInfo<string> {ConstantValue = rightExpr},
                Operator = op == "=" ? Operator.Equal : Operator.Unequal
            };

            var builder = new ContainerBuilder();
            builder.RegisterModule<BpcAutofacModule>();

            var container = builder.Build();
            var context = container.Resolve<IComponentContext>();

            var factory = new ExprFactory(context);

            var expr = factory.Create(_expr);
            _target = context.Resolve<ICondition>(new TypedParameter(typeof(IExpr<bool>), expr));
        }


        [When(@"condition evaluates")]
        public void WhenConditionEvaluates()
        {
            _result = _target.Evaluate(null);
        }

        [Then(@"the condition return (.*)")]
        public void ThenTheConditionReturn(bool expectedResult)
        {
            Assert.AreEqual(expectedResult, _result);
        }

    }
}
