﻿using System;
using Bpc.Contracts.EvalEngine.Info;

namespace Bpc.Tests.EvalEngine.Mocks
{
    internal class ConstantExprInfo<T> : IConstantExprInfo<T>
    {
        public T ConstantValue { get; set; }
        
        public Type ResultType => typeof(T);
    }
}