﻿# Bpc.Tests
This project contains SpecFlow tests for the Bpc project.

The UserInteraction folder contains the tests for the Client. It contains the spec for how the app should interact with the user, regardless of the presentation framework (e.g. web, WPF, etc.).