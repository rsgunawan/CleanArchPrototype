﻿using Bpc.Contracts.Users;

namespace Bpc.Tests.Common
{
    internal class UserMock : IUser
    {
        public int Id { get; }
        public string Name { get; }
        public bool CanCreateBpc { get; }

        public UserMock(int id, string name, bool canCreateBpc)
        {
            Id = id;
            Name = name;
            CanCreateBpc = canCreateBpc;
        }
    }
}
