﻿using AutoMapper;
using Bpc.Contracts.Checklist;
using Service.DataContracts;
using Service.Users;

namespace Service
{
    public class ServiceAutoMapperProfile : Profile
    {
        public ServiceAutoMapperProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<IChecklist, ChecklistDto>();
            CreateMap<IQuestion, QuestionDto>();
            CreateMap<IAnswer, AnswerDto>();
        }
    }
}