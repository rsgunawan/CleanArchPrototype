﻿using System.Linq;
using Autofac;
using AutoMapper;
using Bpc.Contracts.Users;
using Bpc.Users;
using Service.Persistence.Checklist;
using Service.Persistence.Users;

namespace Service.Persistence
{
    class ServicePersistenceAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<AppDbContext>()
                .OnActivating(args => SeedUsers(args.Instance))
                .InstancePerLifetimeScope();

            builder.RegisterType<UserProviderEf>().As<IUserPersistor>().InstancePerLifetimeScope();
            builder.RegisterType<ChecklistProviderEf>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<ServicePersistenceAutoMapperProfile>().As<Profile>();
        }

        private void SeedUsers(AppDbContext dbContext)
        {
            // seed the in-memory DB.
            if (!dbContext.Users.Any())
            {
                var userEntities = new[]
                {
                    new UserEntity(0, "BPC User 0 (ef)", true),
                    new UserEntity(0, "No BPC User 0 (ef)", false),
                    new UserEntity(0, "BPC User 1 (ef)", true),
                    new UserEntity(0, "No BPC User 1 (ef)", false),
                    new UserEntity(0, "BPC User 2 (ef)", true),
                    new UserEntity(0, "BPC User 3 (ef)", true)
                };

                dbContext.Users.AddRange(userEntities);

                var checklistEntities = new[]
                {
                    new ChecklistEntity
                    {
                        Id = 0,
                        User = userEntities[0],
                        Questions = new []
                        {
                            new QuestionEntity { Order = 0, Text = "Question 1", Answer = new AnswerEntity { Yes = false, Notes="Hello" }},
                            new QuestionEntity { Order = 1, Text = "Question 2" },
                            new QuestionEntity { Order = 2, Text = "Question 3" },
                            new QuestionEntity { Order = 3, Text = "Question 4" },
                            new QuestionEntity { Order = 4, Text = "Question 5" },
                            new QuestionEntity { Order = 5, Text = "Question 6" },
                            new QuestionEntity { Order = 6, Text = "Question 7" },
                            new QuestionEntity { Order = 7, Text = "Question 8" },
                            new QuestionEntity { Order = 8, Text = "Question 9" },
                        }
                    },
                };

                dbContext.Checklists.AddRange(checklistEntities);

                dbContext.SaveChanges();
            }
        }
    }
}