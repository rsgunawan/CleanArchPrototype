﻿using System.Collections.Generic;
using AutoMapper;
using Bpc.Contracts.Users;
using Bpc.Users;
using Service.Users;

namespace Service.Persistence.Users
{
    class UserProviderEf : IUserPersistor
    {
        private readonly AppDbContext _userContext;
        private readonly IMapper _mapper;

        public UserProviderEf(AppDbContext userContext, IMapper mapper)
        {
            _userContext = userContext;
            _mapper = mapper;
        }

        public IEnumerable<IUser> LoadUsers()
        {
            var raw = _userContext.Users;
            var output = _mapper.Map<IEnumerable<User>>(raw);
            return output;
        }
    }


}