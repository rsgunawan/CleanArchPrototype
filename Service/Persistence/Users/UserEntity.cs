﻿using System.Diagnostics;

namespace Service.Persistence.Users
{
    [DebuggerDisplay("{Id} {Name} {CanCreateBpc}")]
    class UserEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public bool CanCreateBpc { get; set; }

        public UserEntity()
        { }

        public UserEntity(int id, string name, bool canCreateBpc)
        {
            Id = id;
            Name = name;
            CanCreateBpc = canCreateBpc;
        }
    }
}