﻿namespace Service.Persistence.Checklist
{
    class AnswerEntity
    {
        public int Id { get; set; }
        public bool? Yes { get; set; }
        public string Notes { get; set; }

        public QuestionEntity Question { get; set; }
    }
}