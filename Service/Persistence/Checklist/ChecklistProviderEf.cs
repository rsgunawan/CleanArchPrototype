﻿using System.Linq;
using AutoMapper;
using Bpc.Checklist;
using Bpc.Contracts.Checklist;
using Bpc.Contracts.Users;
using Bpc.Users;
using Microsoft.EntityFrameworkCore;

namespace Service.Persistence.Checklist
{
    class ChecklistProviderEf : IChecklistPersistor
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;

        public ChecklistProviderEf(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IChecklist Save(IUser user, IChecklist toSave)
        {
            throw new System.NotImplementedException();
        }

        public IChecklist Load(IUser user)
        {
            if (user == null)
                return null;

            var raw = _context.Checklists
                .Include(checklist => checklist.Questions)
                .ThenInclude(question => question.Answer)
                .FirstOrDefault(c => c.User.Id == user.Id);
            var output = _mapper.Map<IChecklist>(raw);
            return output;
        }

        public IAnswer LoadAnswer(IUser user, int answerId)
        {
            if (user == null)
                return null;

            var raw = _context.Answers
                .FirstOrDefault(a => a.Id == answerId);
            var output = _mapper.Map<IAnswer>(raw);
            return output;
        }

        public IAnswer SaveAnswer(IUser user, IAnswer newAnswer)
        {
            if (user == null || newAnswer == null)
                return null;

            var raw = _context.Answers
                .FirstOrDefault(a => a.Id == newAnswer.Id);
            _mapper.Map(newAnswer, raw);
            _context.SaveChanges();

            var output = LoadAnswer(user, newAnswer.Id);
            return output;
        }
    }
}