﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using AutoMapper;
using Bpc.Checklist;
using Bpc.Contracts.Checklist;
using Bpc.Contracts.Users;
using Bpc.Users;
using Service.DataContracts;
using Service.Users;

namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "BpcService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select BpcService.svc or BpcService.svc.cs at the Solution Explorer and start debugging.
    public partial class BpcService : IBpcService
    {
        private readonly IUserProcessor _userProcessor;
        private readonly IMapper _mapper;
        private readonly IChecklistProcessor _checklistProcessor;

        public BpcService(IUserProcessor userProcessor, IMapper mapper, IChecklistProcessor checklistProcessor)
        {
            _userProcessor = userProcessor;
            _mapper = mapper;
            _checklistProcessor = checklistProcessor;
        }

        public IEnumerable<UserDto> GetUsers()
        {
            var raw = _userProcessor.GetUsers();
            var output = _mapper.Map<IEnumerable<UserDto>>(raw);
            return output;
        }
    }
}
