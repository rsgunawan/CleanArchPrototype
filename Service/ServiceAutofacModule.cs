﻿using System.Collections.Generic;
using Autofac;
using AutoMapper;
using Service.Models.Checklist;

namespace Service
{
    public class ServiceAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);


            builder.RegisterType<BpcService>();

            builder.RegisterModule<Bpc.BpcAutofacModule>();
            builder.RegisterModule<Models.ServiceModelsAutofacModule>();
            builder.RegisterModule<Users.ServiceUsersAutofacModule>();
            builder.RegisterModule<Persistence.ServicePersistenceAutofacModule>();

            builder.RegisterType<ServiceAutoMapperProfile>().As<Profile>();

            builder.Register(c =>
            {
                var container = c.Resolve<IComponentContext>();
                var profiles = container.Resolve<IEnumerable<Profile>>();

                var mapperConfig = new MapperConfiguration(cfg =>
                {
                    foreach (var profile in profiles)
                    {
                        cfg.AddProfile(profile);
                    }
                });

                return mapperConfig.CreateMapper();
            }).InstancePerLifetimeScope();
        }
    }
}