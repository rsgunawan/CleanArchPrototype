﻿using Autofac;
using Service.Models.Checklist;

namespace Service.Models
{
    public class ServiceModelsAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);


            builder.RegisterType<Checklist.Checklist>().AsImplementedInterfaces();
            builder.RegisterType<Question>().AsImplementedInterfaces();
            builder.RegisterType<Answer>().AsImplementedInterfaces();
        }
    }
}