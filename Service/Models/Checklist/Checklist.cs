using System.Collections.Generic;
using Bpc.Contracts.Checklist;

namespace Service.Models.Checklist
{
    internal class Checklist : IChecklist
    {
        public int Id { get; }
        public IReadOnlyList<IQuestion> Questions { get; }
        
        public Checklist(int id, IReadOnlyList<IQuestion> questions)
        {
            Id = id;
            Questions = questions;
        }
    }
}