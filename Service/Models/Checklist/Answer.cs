using Bpc.Contracts.Checklist;

namespace Service.Models.Checklist
{
    internal class Answer : IAnswer
    {
        public Answer(int id, bool? yes, string notes)
        {
            Id = id;
            Yes = yes;
            Notes = notes;
        }

        public int Id { get; }
        public bool? Yes { get; }
        public string Notes { get; }
    }
}