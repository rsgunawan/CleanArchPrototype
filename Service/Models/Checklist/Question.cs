using Bpc.Contracts.Checklist;

namespace Service.Models.Checklist
{
    internal class Question : IQuestion
    {
        public Question(int order, string text)
        {
            Order = order;
            Text = text;
        }

        public int Order { get; }
        public string Text { get; }

        public IAnswer Answer { get; set; }
    }
}