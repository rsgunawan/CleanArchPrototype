using System.Runtime.Serialization;

namespace Service.DataContracts
{
    [DataContract]
    public class UserDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool CanCreateBpc { get; set; }
    }
}