using System.Collections.Generic;

namespace Service.DataContracts
{
    public class ChecklistDto
    {
        public int Id { get; set; }
        public IEnumerable<QuestionDto> Questions { get; set; }
    }


    public class QuestionDto
    {
        public int Order { get; set; }
        public string Text { get; set; }
        public AnswerDto Answer { get; set; }
    }

    public class AnswerDto
    {
        public int Id { get; set; }
        public bool? Yes { get; set; }
        public string Notes { get; set; }
    }
}