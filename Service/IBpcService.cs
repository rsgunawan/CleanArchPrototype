﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Service.DataContracts;

namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IBpcService
    {
        [OperationContract]
        IEnumerable<UserDto> GetUsers();

        #region Checklist

        [OperationContract]
        ChecklistDto GetChecklistForUser(int userId);


        [OperationContract]
        ChecklistDto CreateChecklistForUser(int userId);

        [OperationContract]
        AnswerDto SaveAnswer(int userId, int answerId, bool? yes, string notes);

        #endregion
    }
}
